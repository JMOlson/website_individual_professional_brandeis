<!-- Primary Page Layout - container div opened in header -->
<?php include 'header.php'; ?>

<main class="row">
    <?php include 'sidebar.php'; ?>
    <section class="one-half column">
    <h3>What's New</h3>
    <h4>Brandeis ICorps 2020</h4>
    <p>Our project building a new maze was selected for the NSF ICorps program this spring! We'll be working to learn more about the potential of commercializing our work and some business basics.</p>
    <h4>New Preprint Available!</h4>
    <p>Our new paper on M2 (rat premotor) activity during action planning and execution is available on the biorxiv site. <a href="https://www.biorxiv.org/content/10.1101/776765v3" target="_blank">Check it out here!</a></p>
    <h4>Home for the 4th!</h4>
    <p>Some important and much needed family time!</p>
    <img src="./_assets/AlaskaMtns.jpg" alt="A view of the mountains in Denali National Park - Credit: Natalie Tongprasearth" title="Credit: Natalie Tongprasearth">
    <h4>T32!</h4>
    <p>I have been appointed as a fellow on Eve Marder's postdoctoral training fellowship. Honored to be selected and looking forward to the additional oppotunities to grow my computational skills.</p>
    <h4>I'm joining Jadhav Lab!</h4>
    <p>Super excited to announce I'll be joining Shantanu Jadhav's lab as a postdoctoral associate. A lot of great people producing great work  looking at reactivation and its role in learning. I'll be looking to learn some of these same techniques and apply them to subiculum and related regions.</p>
    <h4>Alaska!</h4>
    <p>Nat and I trekked up North to Alaska for Geoff and Emily's wedding. What a great wedding it was, including a chairlift ride to the top of the mountain before the dance party! We got some exploring in as well, checking out glaciers and Denali.</p>
    <img src="./_assets/AlaskaMtns.jpg" alt="A view of the mountains in Denali National Park - Credit: Natalie Tongprasearth" title="Credit: Natalie Tongprasearth">
    <h4>SfN Roadtrip!</h4>
    <p>After a trip to all of the exciting science of SfN, Nat and I took off from San Diego to drive all the way to Boston! Along the way, we stopped in Kansas for my sister's wedding. It was wonderful to see extended family and celebrate with Becca and Joe! And the car only broke down once.</p>
    <img src="./_assets/Zion.jpg" alt="A view of Zion National Park - Credit: Natalie Tongprasearth" title="Credit: Natalie Tongprasearth">
    <h4>F32!</h4>
    <p>Stoked to announce that I just received BRAIN Initiative F32 funding on our work in valence encoding in the amygdala!</p>
    <h4>Amygdala GRC</h4>
    <p>Amygdala GRC was a wonderful meeting! I was able to quickly get up to speed on the current state of research and meet lots of great and kind researchers! Excited to be joining the field.</p>
    <img src="./_assets/AmygdalaGRCGroup.jpg" alt="The Amygdala GRC 2017 Group! - Crdeit GRC" title="Credit: GRC">>
    <h4>Tye Lab</h4>
    <p>I'm happy to announce that now that I have finished at UCSD, I'll be moving on to study valence processing in the amygdala and related structures with Kay Tye and colleagues at MIT!</p>
    <h4>Jacob M Olson, PhD!</h4>
    <p>After a meandering journey through 2 labs and a break, I'm finally a doctor! Thank you to everyone who has supported me throughout out this time. Now, onward!</p>
    <h4>Axis Tuning in Subiculum Paper Out!</h4>
    <p>After some helpful suggestions from reviewers, our paper on axis mapping in the dorsal subiculum is now published!<a href="./research.php">Get it here !</a></p></p>
    <h4>New Preprint Available!</h4>
    <p>Our new paper on axis mapping in the dorsal subiculum is available on the biorxiv site. <a href="http://biorxiv.org/content/early/2016/04/27/050641" target="_blank">Check it out here!</a></p>
    <h4>FENS 2016</h4>
    <p>Immediately after iNAV, I will be presenting additional subiculum findings at FENS, the annual meeting of the European Society of Neuroscience. I anticipate this conference will be a great opportunity to interact with labs and researchers I normally have less exposure to. Updates to come!</p>
    <h4>iNAV 2016</h4>
    <p>In late June I will be traveling to Bad Gastein, Austria, for the iNav conference, a specialized conference on spatial navigation. I will present our recent findings on spatial representations in the dorsal subiculum. I am very excited about the opportunity to spend the week with fellow researchers interested in this problem and hearing about the most recent research and hypotheses regarding this problem. I'll post about the experience afterward!</p>
    <h4>KAVLI Institute of Brain and Mind Symposium on Innovative Research</h4>
    <p>On Saturday, May 7th I presented the results of our research that was generously funded by KIBM this year. The symposium was filled with interesting and novel lines of research being conducted here at UCSD. I was particulary interested in some of the new techniques being researched, including 3D models of individual neurons and nanorobots for neuron targeting and stimulation. Let's hope they advance to the point that we can utilize these techniques!</p>
    <h4>SfN 2015</h4>
    <p>I will be presenting my most recent data from the subiculum.</p>
  </section>
  <aside class="three columns">

  </aside>
</main>
