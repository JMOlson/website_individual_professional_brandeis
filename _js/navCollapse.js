window.onload = function() {
    //toggle class utility function
    classToggle = function(element, tclass) {
        var classes = element.className,
            pattern = new RegExp( tclass );
        var hasClass = pattern.test( classes );
        //toggle the class
        classes = hasClass ? classes.replace( pattern, '' ) : classes + ' ' + tclass;
        element.className = classes.trim();
    }

    var nav = document.getElementsByTagName('nav')[0];
    var navItem = nav.getElementsByTagName('li')[0];
    var navList = nav.getElementsByTagName('ul')[0];

    //Is it floated?
    var floated = navItem.currentStyle ? el.currentStyle['float'] :
        document.defaultView.getComputedStyle(navItem,null).getPropertyValue('float');

    if(floated != 'left') {
        var collapse = document.getElementById('nav-collapse');
        classToggle(navList, 'hide');
        classToggle(collapse, 'active');

        collapse.onclick = function() {
            classToggle(navList, 'hide');
            return false;
        }
    }
}
