<?php
$seconds_to_cache = 7200;
$ts = gmdate("D, d M Y H:j:s", time() + $seconds_to_cache) . " GMT";
header("Cache-Control: max-age=$seconds_to_cache");
header("Expires: $ts");

echo file_get_contents('http://www.google-analytics.com/analytics.js');
?>
