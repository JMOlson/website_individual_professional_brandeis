<!-- Primary Page Layout - container div opened in header -->
<?php include 'header.php'; ?>

<main class="row">
    <?php include 'sidebar.php'; ?>
    <section class="one-half column">
        <h4>Dissertation Overview</h4>
        <p>In my dissertation research under <a href="http://www.dnitz.com/"target="_blank">Douglas A. Nitz</a> at UC San Diego, I investigated the spatial correlates of multiple regions hypothesized to be crucial in spatial navigation. To do this, I used <i>in vivo</i> electrophysiology of behaving rats navigating a complex environment with many paths and potential reward locations. In this project, I recorded from M2 (a premotor subregion of mPFC), dorsal subiculum, CA1, posterior parietal cortex, and superior colliculus. Our results are pointing towards a potential pathway for information flow during spatial navigation. Read more about particular projects below.</p>
        <h5>Generalization in the Dorsal Subiculum</h5>
        <p>The subiculum is one of the primary outputs of CA1 to cortex, especially associative cortex such as parietal and retrosplenial cortex. Despite this, few experiments have been conducted in subiculum, especially compared to its extremely popular neighbor, CA1. Previous research has described subicular neurons in open field foraging as very similar to CA1, but with broader firing fields (Sharp 1994) that also generalize more to the same relative locations of similar environments (Sharp, 1997).  When placed on a U-shaped track results were consistant with this account (Kim et al., 2012). In our work, utilizing a multi-path and multi-turn with 4 possible reward sites, a much more diverse activity pattern emerged. Overall, neural representations do seem to fit with this idea of generalization, but the manner in which they do so is quite different, with neurons that generalize across spaces that are spatially and behaviorally distinct, but similar in their function (i.e. halfway back on a path to a reward). We also see a subset of subiculum neurons that fire when the animals run in either of two opposite directions. That is they seem to mark the axis of travel of the animal! We've published the axis tuning (link below) and hope to have the remainder out by the end of 2020. In the futre, I am excited to investigate more about this region and how these representations interface with two regions subiculum is highly interconnected with: CA1 and entorhinal cortex.</p>
        <h6>Related Published Works</h6>
        <dl>
          <dt><a href="./_assets/2017_NatNeuro.pdf" target="_blank">Subiculum Neurons Map the Current Axis of Travel</a> - <a href="./_assets/2017_NatNeuro_Supplemental.pdf" target="_blank">Supplemental including BVC analysis</a></dt>
          <dd>- Axis Tuning in the dorsal subiculum, as well as a model of boundary vector cells and their prevalence in our subiculum dataset.</dd>
          <dt>Comparison of CA1 and Subiculum on a Complex Maze</dt>
          <dd>- In progress!</dd>
        </dl>
        <h4>M2 and the Planning and Execution of Action</h4>
        <p>M2, also known as medial precentral cortex (MPC), is a brain region in medial prefrontal cortex (mPFC) of the rat. It is hypothesized to be highly involved with decision making and action planning. Unlike the other subregions of mPFC, however, MPC has garnered less attention, largely due to a lack of correlates to decision making. We are examining MPC neural activity patterns in the context of our multi-turn navigational task. The task structure allows for the teasing apart of spatial and action correlates as well as assessing when action activity is maximal with respect to the action (such as before, during, or after). We can also inspect the sequence of actions to see if MPC encodes turns in a sequence of actions differantly, a known property of premotor cortex in non-human primates.</p>
        <h6>Related Published Works</h6>
        <dl>
          <dt><a href="https://www.biorxiv.org/content/10.1101/776765v3" target="_blank">Secondary Motor Cortex Transforms Spatial Information into Planned Action during Navigation</a></dt>
          <dd>- This paper shows how many spatial variables are encoded in M2, but at a level that is dwarfed by motor (turn) correlates. I hope a takeaway is the importance of measuring many variables, as if one didn't measure the motor correlates, they would say that this is a spatial region!</dd>
        </dl>
        <h4>Tye Lab - Mechanisms of Valence Encoding in the Amygdala</h4>
        <p>In Kay Tye's lab, I joined a team lead by Praneeth Namburi investigating the role of the neuropeptide neurotensin in the ability of the rodent amygdala to encode valence (positive versus negative). This was a large sprawling project with many interesting results. Unfortunately, when the lab moved to the Salk Institute and I moved on to Jadha Lab, work remained to complete our understanding in a way ready to publish. We'll see if an interested researcher picks it up and finishes the work! Along the way, though, I picked up many skills such as optogenetic experimentation, calcium imaging, and working with mice.</p>
        <h4>Jadhav Lab - Learning & the Subiculum</h4>
        <p>My work in Shantanu Jadhav's lab is going to start out focusing on an old region of interest of mine, subiculum. We will take the high neuron count simultaneous recordings that Jadhav lab uses to look at learning in this region, something that had eluded us previoiusly. We'll see where the data takes us from there!</p>
    </section>
    <aside class="three columns">

    </aside>
</main>

<!-- Note: columns can be nested, but it's not recommended since Skeleton's grid has %-based gutters, meaning a nested grid results in variable with gutters (which can end up being *really* small on certain browser/device sizes) -->

<?php include 'footer.php'; ?>
</body>
</html>
