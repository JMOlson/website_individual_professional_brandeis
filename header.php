<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- SEO Stuff -->
    <title>Jacob M Olson</title>
    <meta name="description" content="The homepage and web presence of Jacob Olson - neuroscientist, data analyst, web developer, and enthusiast of music, baseball, and hoops.">
    <meta name="keywords" content="Jacob M Olson, Jacob Olson, Jake Olson, Olson, neuroscience, systems neuroscience, computational neuroscience, behavioral neuroscience, Jadhav, Nitz, Tye, spatial navigation, navigation, premotor, m2, subiculum, data analysis, axis, axis cells, AdaptaMaze, Brandeis, Braneis University, MIT, Massachusetts Institute of Technology, UCSD, UC San Diego, Cognitive Science, research, teaching">
    <meta name="author" content="Jacob Olson">

    <!-- Stylesheet Links -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/skeleton.css">
    <link rel="stylesheet" href="css/customStyles.css">

    <!-- Javascript Links -->

    <!-- FONT –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
    <!-- Example to link in an image -->
    <!-- link rel="icon" type="image/png" href="images/favicon.png" -->

    <?php include_once("howManyPeople.php") ?>
</head>
<body>

<div class="container">
    <header class="row banner primaryBG">
        <div class="two-thirds column">
            <h1><a href="./index.php" >Jacob Olson</a></h1>
        <nav id="stickyNav" role="navigation">
            <a href="#nav" id="nav-collapse"><image src="./_assets/nav_List_Button.svg" alt="Navigation Menu Button"></a>
            <ul>
                <li><a href="./index.php">Home</a></li>
                <li><a href="./myNews.php">My News</a></li>
                <li><a href="./research.php">Research</a></li>
                <li><a href="./teaching.php">Teaching</a></li>
                <li><a href="./personal.php">Personal</a></li>
                <li><a href="./_assets/20200409_CV_JMO_Web.pdf">Vitae</a></li>
            </ul>
        </nav>
        </div>
        <figure class="one-third column">
            <img src="./_assets/JMO_Portrait.png" alt="Jacob Olson Portrait">
        </figure>
    </header>
