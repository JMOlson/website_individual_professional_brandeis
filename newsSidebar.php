<aside class="three columns" id="update_bar">
  <h3>What's New</h3>
  <h4>Brandeis ICorps 2020</h4>
  <p>Our project building a new maze was selected for the NSF ICorps program this spring! We'll be working to learn more about the potential of commercializing our work and some business basics.</p>
  <h4>New Preprint Available!</h4>
  <p>Our new paper on M2 (rat premotor) activity during action planning and execution is available on the biorxiv site. <a href="https://www.biorxiv.org/content/10.1101/776765v3" target="_blank">Check it out here!</a></p>
  <h4>Home for the 4th!</h4>
  <p>Some important and much needed family time!</p>
  <img src="./_assets/AlaskaMtns.jpg" alt="A view of the mountains in Denali National Park - Credit: Natalie Tongprasearth" title="Credit: Natalie Tongprasearth">
  <h4>T32!</h4>
  <p>I have been appointed as a fellow on Eve Marder's postdoctoral training fellowship. Honored to be selected and looking forward to the additional oppotunities to grow my computational skills.</p>
  <h4>I'm joining Jadhav Lab!</h4>
  <p>Super excited to announce I'll be joining Shantanu Jadhav's lab as a postdoctoral associate. A lot of great people producing great work  looking at reactivation and its role in learning. I'll be looking to learn some of these same techniques and apply them to subiculum and related regions.</p>
</aside>
