<!-- Primary Page Layout - container div opened in header -->
<?php include 'header.php'; ?>

<main class="row">
    <?php include 'sidebar.php'; ?>
    <section class="one-half column">
        <p>Hello and welcome! I am a Postdoctoral Fellow under the guidance of <a href="http://www.bio.brandeis.edu/jadhavlab/" target="_blank">Shantanu Jadhav</a> in the Neuroscience Program and Psychology Department at Brandeis University. I am especially curious about brains and cognition from a computation standpoint - how do we effortlessly accomplish so many tasks that our current computers are incapable of? My work utilizes spatial navigation in rats as a systems neuroscience problem to investigate the network and circuitry that accomplishes this complex task. For fun, I like to play sports (although not near as much as I used to), bake, play boardgames, and enjoy going to live music.</p>
    </section>
    <?php include 'newsSidebar.php'; ?>
</main>

<!-- Note: columns can be nested, but it's not recommended since Skeleton's grid has %-based gutters, meaning a nested grid results in variable with gutters (which can end up being *really* small on certain browser/device sizes) -->

<?php include 'footer.php'; ?>
</body>
</html>
