<!-- Primary Page Layout - container div opened in header -->
<?php include 'header.php'; ?>

<main class="row">
    <?php include 'sidebar.php'; ?>
    <section class="one-half column" id="teaching">
        <h5>Previous Courses Taught</h5>
        <p>COGS 109: Modeling and Data Analysis (Summer 2015, Fall 2016, Summer 2016) <a href="./_assets/COGS_109_Syllabus_Su15.pdf" target="_blank">Syllabus</a></p>
        <h5>Teaching Assistant Experience</h5>
        <ul>
            <li>COGS 1: Introduction to Cognitive Science</li>
            <li>COGS 14A: Introduction to Research Methods</li>
            <li>COGS 17: Neurobiology of Cognition</li>
            <li>COGS 18: Introduction to Programming for Cognitive Science</li>
            <li>COGS 101B: Learning, Memory, and Attention</li>
            <li>COGS 101C: Language</li>
            <li>COGS 107A: Neuroanatomy and Physiology</li>
            <li>COGS 107B: Systems Neuroscience</li>
            <li>COGS 109: Modeling and Data Analysis</li>
        </ul>
    </section>
</main>

<!-- Note: columns can be nested, but it's not recommended since Skeleton's grid has %-based gutters, meaning a nested grid results in variable with gutters (which can end up being *really* small on certain browser/device sizes) -->

<?php include 'footer.php'; ?>
</body>
</html>
