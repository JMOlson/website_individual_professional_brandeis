<!-- Primary Page Layout - container div opened in header -->
<?php include 'header.php'; ?>

<main class="row">
    <?php include 'sidebar.php'; ?>
    <section class="one-half column">
        <h3>A Little Bit About Me</h3>
        <p>While my research is very important to me, a lot of other things are too! I grew up in small towns in the midwest (Radcliffe, IA, and Ellsworth, KS), an experience that is very different from most of the people I run into these days. I have a big family including 1 brother, 2 sisters, and now 3 nephews! I attended the University of Kansas for my undergraduate degree and loved my four years there, learning a lot and making great friends. Afterwards I moved to San Diego to begin my PhD at UC San Diego. I love San Deigo - the diversity, the food, the weather, the ocean. Yes, I have tried surfing. It's fun, but no, I'm definitely not a surfer. Some of my other non-neuroscience interests include:</p>
        <h4>Baseball, the Royals, and Sabermetrics</h4>
        <p>I grew up playing and eventually umpiring baseball and I have always enjoyed the sport. During the summer of my junior year at the University of Kansas, my housemates and I always used to have the Royals game on in the background while going about our evening. While they rarely won, I developed a bond with the team and have been faithfully following them ever since. One of my favorite websites is FanGraphs, and I highly recommend it for any baseball fan to understand more about the game. I would like to delve into Sabermetrics in my spare time, but have not completed a project worth publication to date.</p>
        <h4>Playing Basketball and Soccer</h4>
        <p>I've been playing basketball all of my life and am always ready for a game of pickup. Since moving to Boston, this has really dropped off, unfortunately. I'm hoping to pick it back up now that I'm at Brandeis. I am a KU alum and huge fan (Rock Chalk!) but also support the lovable loser of Iowa State, my parents' alma mater.</p>
        <p>Soccer is a different story - I did not play growing up, and while I really enjoy playing, my best quality is probably that I'm willing to run a lot. I still like to play however! I came to love the game when watching the fast passing and ball movement style played most notably by Barcelona. The vision and coordination has many parallels to good basketball and is beautiful to watch when a team is moving in rhythm.</p>
        <h4>Live Music</h4>
        <p>I love going to concerts! Some of my favorites:</p>
        <ul>
            <li>Bonnaroo</li>
            <li>San Fermin</li>
            <li>Niki & the Dove</li>
            <li>Lord Huron</li>
            <li>Dr. Dog</li>
            <li>CHVRCHES</li>
        </ul>
    </section>
    <aside class="three columns">
        <img src="./_assets/KSSunset.png" alt="A view of the sunset from my parents house near Ellsworth, KS">
        <img src="./_assets/Family4th.jpg" alt="A bunch of my family together celebrating the 4th">
        <img src="./_assets/TyeFriendsAtC+MWed.jpg" alt="Tye Lab friends at Chris and Miranda's wedding">
        <img src="./_assets/Champs.jpg" alt="One of the last Y championship teams">
        <img src="./_assets/UCSD.jpg" alt="UCSD friends">
        <img src="./_assets/KUFriends.jpg" alt="Graduation day photo of my KU friends and I">
    </aside>
</main>

<!-- Note: columns can be nested, but it's not recommended since Skeleton's grid has %-based gutters, meaning a nested grid results in variable with gutters (which can end up being *really* small on certain browser/device sizes) -->

<?php include 'footer.php'; ?>
</body>
</html>
